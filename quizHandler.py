import requests,json
def main():
    server_ip = str(requests.get('https://api.ipify.org').text)
    sendData = {
        "quiz_slug" : "rds_read_repica",
        "callback_url" : "http://"+server_ip+":8887/check"
    }
    data = (requests.post("https://pizg9b4aea.execute-api.us-east-1.amazonaws.com/default/new_token",json=sendData))
    token = json.loads(data.text)["token"]

    with open("/usr/local/token.log", "x") as data_file:
        data_file.write(token)

    with open("./templates/index.html", "r") as html_file:
        newText = html_file.read().replace("$(tokenID)", token)
    with open("./templates/index.html", "w") as html_file:
        html_file.write(newText)

main()