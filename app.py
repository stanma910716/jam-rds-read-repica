from flask import Flask,url_for,request
import mysql.connector
from mysql.connector import errorcode
import requests
import json

app = Flask(__name__)

@app.route("/check",methods=["POST"])
def home():
    sql_data = (request.get_json())
    with open("/usr/local/token.log", "r") as data_file:
        token = str(data_file.read())
    Success = True
    readDB = mysql.connector.connect(
        host = sql_data["replica_endpoint"],
        user = sql_data["username"],
        password = sql_data["password"],
        database = sql_data["db_name"]
    )
    writeDB = mysql.connector.connect(
        host = sql_data["master_endpoint"],
        user = sql_data["username"],
        password = sql_data["password"],
        database = sql_data["db_name"]
    )
    read_cursor = readDB.cursor()
    write_cursor = writeDB.cursor()

    try:
        write_cursor.execute("CREATE TABLE if not exists `test` (`id` INT(225) NOT NULL AUTO_INCREMENT,`name` TEXT NOT NULL,PRIMARY KEY (`id`));")
    except mysql.connector.Error as err:
        err.msg
        Success = False
    if(Success == True):
        try:
            write_cursor.execute("INSERT INTO `test` (`name`) \r\nVALUES ('"+str(token)+"');")
            writeDB.commit()
        except mysql.connector.Error as err:
            Success == False
    if(Success == True):
        try:
            read_cursor.execute("SELECT * FROM test")
            result = read_cursor.fetchall()
        except mysql.connector.Error as err:
            Success = False
        for x in result:
            if(str(x[1]) == str(token)):
                Success = True
            else:
                Success = False
    if(Success == True):
        try:
            read_cursor.execute("INSERT INTO `test` (`name`) \r\nVALUES ('"+str(token)+"');")
            readDB.commit()
        except mysql.connector.Error as err:
            if(err.errno == 1290):
                Success = True
            else:
                Success = False
    return_data = {
        "success" : Success
    }
    return return_data

if __name__ == "__main__":
    app.run(host="0.0.0.0",port=8887)