from flask import Flask

app = Flask(__name__)

@app.route("/token")
def token():
    with open("/usr/local/token.log", "r") as data_file:
        token = str(data_file.read())
    return render_template("index.html")

if __name__ == "__main__":
    app.run(host="0.0.0.0",port=80)